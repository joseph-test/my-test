package add

import (
	"testing"
)

func TestAdd(t *testing.T) {
	a, b := 1, 1
	if My_add(a, b) != 2 {
		t.Errorf("1 + 1 should be 2")
	}
}
