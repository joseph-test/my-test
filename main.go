package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/joseph-test/my-test/add"
	"net/http"
	"strconv"
)

func main() {
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	s := strconv.Itoa(add.My_add(1, 1))

	e.GET("/", func(c echo.Context) error {

		return c.String(http.StatusOK, "1 + 1 = "+s)
	})

	e.Logger.Fatal(e.Start(":1323"))

}
