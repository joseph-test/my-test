FROM golang:1.15.3-alpine
WORKDIR /add
ADD . /add
RUN cd /add && go build -o main
EXPOSE 1323
ENTRYPOINT ./main